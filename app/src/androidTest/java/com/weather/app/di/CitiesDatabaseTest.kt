package com.weather.app.di

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CitiesDatabaseTest {

    @Test
    fun getInstance() {
        Assert.assertEquals(
            "CitiesDatabase_Impl",
            CitiesDatabase.getInstance(ApplicationProvider.getApplicationContext()).javaClass
                .simpleName
        )
    }
}