package com.weather.app.domain.usecase.whether

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.weather.app.di.CitiesDatabase
import com.weather.app.di.CityDao
import com.weather.app.entity.City
import com.weather.app.testdatafactories.CityTestDataFactory
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subscribers.TestSubscriber
import org.junit.After
import org.junit.Before
import org.junit.Test

class CityRepoImplTest {

    private lateinit var cityDao: CityDao
    private lateinit var citiesDb: CitiesDatabase

    @Before
    fun setUp() {
        citiesDb = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            CitiesDatabase::class.java
        )
            .build()
        cityDao = citiesDb.cityDao
    }

    @Test
    fun testGetAllCities() {
        val TEST_NAME = "test"
        val testCity: City = CityTestDataFactory.valid
        testCity.name = TEST_NAME
        cityDao.insert(testCity)
        val allTestCities: Flowable<List<City>> = cityDao.all

        val testSubscriber: TestSubscriber<List<City>> = allTestCities
            .test()
            .awaitCount(1)

        testSubscriber
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue { value -> value[0].name == TEST_NAME }
        testSubscriber.isCancelled
    }

    @After
    fun closeDb() {
        citiesDb.close()
    }
}