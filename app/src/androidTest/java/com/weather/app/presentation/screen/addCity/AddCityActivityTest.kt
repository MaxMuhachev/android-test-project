package com.weather.app.presentation.screen.addCity

import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.presentation.screen.editCities.CitiesItemAdapterForEditCities
import com.weather.app.presentation.screen.editCities.EditCitiesActivity
import com.weather.app.testdatafactories.CityTestDataFactory
import com.weather.app.utilit.I18n
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AddCityActivityTest {

    @Rule
    @JvmField
    var editRule: ActivityTestRule<EditCitiesActivity> =
        ActivityTestRule(EditCitiesActivity::class.java)

    @Rule
    @JvmField
    var addRule: ActivityTestRule<AddCityActivity> = ActivityTestRule(AddCityActivity::class.java)


    private lateinit var activity: EditCitiesActivity
    private lateinit var recyclerViewById: RecyclerView
    private lateinit var addCityActivity: AddCityActivity

    @Before
    fun setUp() {
        activity = editRule.activity
        addCityActivity = addRule.activity
        recyclerViewById = activity.findViewById(R.id.recyclerViewEditCities)
    }

    @Test
    fun createRecyclerViewAndAdapter() {
        Assert.assertNotNull(recyclerViewById)
        Assert.assertTrue(recyclerViewById.adapter is CitiesItemAdapterForEditCities)
    }

    @Test
    fun whenAddCity_ThenAddToEditActivity() {
        val validCity: City = CityTestDataFactory.valid
        val testEditText: EditText = addCityActivity.findViewById(R.id.editCityName)
        val resultText: TextView = addCityActivity.findViewById(R.id.resultCity)
        val adapter: CitiesItemAdapterForEditCities =
            recyclerViewById.adapter as CitiesItemAdapterForEditCities

        addCityActivity.runOnUiThread { testEditText.setText(validCity.name) }
        Thread.sleep(2000)
        addCityActivity.runOnUiThread { resultText.performClick() }

        Thread.sleep(2000)
        Assert.assertNotNull(adapter)
        Assert.assertEquals(I18n.NAME_TEST_CITY_BANG_RU,
            adapter.getItem(adapter.itemCount - 1).name)
    }
}