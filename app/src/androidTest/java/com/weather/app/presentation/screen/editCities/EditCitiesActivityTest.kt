package com.weather.app.presentation.screen.editCities


import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.presentation.screen.addCity.AddCityActivity
import com.weather.app.testdatafactories.CityTestDataFactory
import com.weather.app.utilit.I18n
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditCitiesActivityTest {
    @Rule
    @JvmField
    var editRule: ActivityTestRule<EditCitiesActivity> =
        ActivityTestRule(EditCitiesActivity::class.java)

    @Rule
    @JvmField
    var addRule: ActivityTestRule<AddCityActivity> = ActivityTestRule(AddCityActivity::class.java)


    private lateinit var activity: EditCitiesActivity
    private lateinit var recyclerViewById: RecyclerView
    private lateinit var addCityActivity: AddCityActivity

    @Before
    fun setUp() {
        activity = editRule.activity
        addCityActivity = addRule.activity
        recyclerViewById = activity.findViewById(R.id.recyclerViewEditCities)
    }

    @Test
    fun createRecyclerViewAndAdapter() {
        Assert.assertNotNull(recyclerViewById)
        Assert.assertTrue(recyclerViewById.adapter is CitiesItemAdapterForEditCities)
    }

    @Test
    fun whenAddCity_ThenAddToEditActivity() {
        val validCity: City = CityTestDataFactory.valid
        val testEditText: EditText = addCityActivity.findViewById(R.id.editCityName)
        val resultText: TextView = addCityActivity.findViewById(R.id.resultCity)
        val adapter: CitiesItemAdapterForEditCities =
            recyclerViewById.adapter as CitiesItemAdapterForEditCities

        addCityActivity.runOnUiThread { testEditText.setText(validCity.name) }
        Thread.sleep(2000)
        addCityActivity.runOnUiThread { resultText.performClick() }

        Thread.sleep(2000)
        Assert.assertNotNull(adapter)
        Assert.assertEquals(I18n.NAME_TEST_CITY_BANG_RU,
            adapter.getItem(adapter.itemCount - 1).name)
    }

    @Test
    fun whenDeleteCity_ThenDeleteToEditActivity() {
        val validCity: City = CityTestDataFactory.valid
        val testEditText: EditText = addCityActivity.findViewById(R.id.editCityName)
        val resultText: TextView = addCityActivity.findViewById(R.id.resultCity)
        val adapter: CitiesItemAdapterForEditCities =
            recyclerViewById.adapter as CitiesItemAdapterForEditCities
        val expectedCount: Int = adapter.itemCount

        addCityActivity.runOnUiThread { testEditText.setText(validCity.name) }
        Thread.sleep(2000)
        addCityActivity.runOnUiThread { resultText.performClick() }

        val viewHolderForLayoutPosition = recyclerViewById.findViewHolderForLayoutPosition(expectedCount - 1)
        Assert.assertNotNull(viewHolderForLayoutPosition)
        val btnDel = viewHolderForLayoutPosition!!.itemView.findViewById<View>(R.id.deleteBtn)
        activity.runOnUiThread { btnDel.performClick() }
        Thread.sleep(3000)

        Assert.assertEquals(expectedCount, adapter.itemCount)
    }

    @Test
    fun whenSetFavorite_ThenUpdateState() {
        val validCity: City = CityTestDataFactory.valid
        val testEditText: EditText = addCityActivity.findViewById(R.id.editCityName)
        val resultText: TextView = addCityActivity.findViewById(R.id.resultCity)
        val adapter: CitiesItemAdapterForEditCities =
            recyclerViewById.adapter as CitiesItemAdapterForEditCities
        val insertIndex: Int = adapter.itemCount - 1

        addCityActivity.runOnUiThread { testEditText.setText(validCity.name) }
        Thread.sleep(2000)
        addCityActivity.runOnUiThread { resultText.performClick() }

        val viewHolderForLayoutPosition = recyclerViewById.findViewHolderForLayoutPosition(insertIndex)
        Assert.assertNotNull(viewHolderForLayoutPosition)
        val btnFavorite = viewHolderForLayoutPosition!!.itemView.findViewById<View>(R.id.favoriteBtn)
        activity.runOnUiThread { btnFavorite.performClick() }
        Thread.sleep(3000)

        Assert.assertEquals(true, adapter.getItem(insertIndex).favorite)
    }
}