package com.weather.app.testdatafactories

import com.weather.app.entity.City

object CityTestDataFactory {
    val valid: City
        get() = City(
            "Bangkok",
            false
        )
}