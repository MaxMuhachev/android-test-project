package com.weather.app

import android.app.Application
import android.content.Context
import com.weather.app.di.AppModule
import com.weather.app.di.CityRepositoryModule
import toothpick.Scope
import toothpick.configuration.ConfigurationHolder
import toothpick.ktp.KTP


open class App : Application() {

    private lateinit var appScope: Scope

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        ConfigurationHolder.configuration.allowMultipleRootScopes()
    }

    override fun onCreate() {
        super.onCreate()
        appScope = KTP.openScope(this)
            .installModules(
                AppModule(this),
                CityRepositoryModule(),
            )

        instance = this
    }


    companion object : App() {
        lateinit var instance: App
            private set

        fun applicationContext() : Context {
            return instance.applicationContext
        }

        fun scope(): Scope {
            return instance.appScope
        }
    }
}