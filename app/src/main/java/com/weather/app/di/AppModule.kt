package com.weather.app.di

import android.content.Context
import com.google.gson.Gson
import com.weather.app.App
import toothpick.config.Module

class AppModule(app: App) : Module() {
    init {
        bind(Context::class.java).toInstance(app)
        bind(Gson::class.java).singleton()
    }
}