package com.weather.app.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.weather.app.BuildConfig
import com.weather.app.entity.City
import com.weather.app.utilit.I18n

@Database(entities = [City::class], version = 3, exportSchema = false)
abstract class CitiesDatabase : RoomDatabase() {
    abstract val cityDao: CityDao

    companion object {
        private var INSTANCE: CitiesDatabase? = null
        fun getInstance(context: Context): CitiesDatabase {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(context, CitiesDatabase::class.java, I18n.NAME_DATABASE)
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                if (INSTANCE!!.cityDao.databaseIsNotEmpty()) {
                    INSTANCE!!.cityDao.insert(City(BuildConfig.NAME_CITY_PERM, true))
                }
            }
            return INSTANCE as CitiesDatabase
        }
    }
}