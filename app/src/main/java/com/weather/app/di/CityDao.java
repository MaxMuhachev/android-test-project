package com.weather.app.di;

import android.database.Cursor;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.weather.app.entity.City;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;

@Dao
public interface CityDao {
    @Query("SELECT * from cities")
    Flowable<List<City>> getAll();

    @Query("SELECT * FROM cities WHERE name = :name")
    List<City> getByName(String name);

    @Query("SELECT count(*) = 0 FROM cities")
    Boolean databaseIsNotEmpty();

    @Query("SELECT * FROM cities")
    List<City> list();

    @Update
    void updateFavoriteState(City city);

    @Delete
    void delete(City city);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(City city);
}
