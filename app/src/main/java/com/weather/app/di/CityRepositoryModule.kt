package com.weather.app.di

import com.weather.app.domain.usecase.whether.CityRepoImpl
import toothpick.config.Module

class CityRepositoryModule : Module() {
    init {
        bind(CityRepoImpl::class.java)
            .singleton()
    }
}