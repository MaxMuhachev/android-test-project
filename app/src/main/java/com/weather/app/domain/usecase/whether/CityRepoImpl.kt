package com.weather.app.domain.usecase.whether

import com.weather.app.App
import com.weather.app.di.CitiesDatabase
import com.weather.app.di.CityDao
import com.weather.app.entity.City
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class CityRepoImpl @Inject constructor() {

    companion object {
        lateinit var cityDao: CityDao
    }
    init {
        cityDao = CitiesDatabase.getInstance(App.applicationContext()).cityDao;
    }

    fun getAllCities(): Flowable<MutableList<City>> {
        return cityDao.all
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun deleteCity(city: City) {
        cityDao.delete(city)
    }

    fun insertCity(city: City) {
        cityDao.insert(city)
    }

    fun updateFavoriteState(city: City) {
        cityDao.updateFavoriteState(city)
    }

    fun getCityByName(name: String): List<City> {
        return cityDao.getByName(name)
    }
}