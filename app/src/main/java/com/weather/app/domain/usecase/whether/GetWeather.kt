package com.weather.app.domain.usecase.whether

import com.weather.app.BuildConfig
import com.weather.app.entity.ResponseWeather
import com.weather.app.entity.WeatherRepo
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject


class GetWeather @Inject constructor() {

    operator fun invoke(q: String): Single<ResponseWeather> {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.WHETHER_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return getFromRepo(q, retrofit)
    }

    private fun getFromRepo(q: String, retrofit: Retrofit): Single<ResponseWeather> {
        return retrofit.create(WeatherRepo::class.java)
            .weatherGet(q, BuildConfig.LANG, BuildConfig.API_KEY)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}