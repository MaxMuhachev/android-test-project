package com.weather.app.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.weather.app.BuildConfig

data class Weather(
    @Expose
    val id: Int,

    @SerializedName("main")
    @Expose
    val main: String,

    @SerializedName("description")
    @Expose
    val description: String,

    @SerializedName("icon")
    @Expose
    var icon: String
) {
    fun getIconLink(): String {
        return if (icon.isEmpty()) {""} else String.format(BuildConfig.IMAGE_URL, icon)
    }
}