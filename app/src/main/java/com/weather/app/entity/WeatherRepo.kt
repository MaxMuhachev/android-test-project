package com.weather.app.entity

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherRepo {
    @GET("weather?units=metric")
    fun weatherGet(
        @Query("q") q: String?,
        @Query("lang") lang: String?,
        @Query("appid") appid: String?
    ): Single<ResponseWeather>
}