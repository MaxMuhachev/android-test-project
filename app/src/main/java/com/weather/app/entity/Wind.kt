package com.weather.app.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Wind (
    @SerializedName("speed") @Expose
    var speed: Float,

    @SerializedName("deg")
    @Expose
    val deg: Int
)