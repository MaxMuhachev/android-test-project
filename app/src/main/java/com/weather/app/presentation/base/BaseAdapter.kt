package com.weather.app.presentation.base

import androidx.recyclerview.widget.RecyclerView
import com.weather.app.presentation.customclicklistener.OnItemClickListener
import java.util.*

abstract class BaseAdapter<VH : RecyclerView.ViewHolder?, T>(items: List<T>?) :
    RecyclerView.Adapter<VH>() {
    private val items: MutableList<T> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener
    override fun getItemCount(): Int {
        return items.size
    }

    private fun refreshRecycler() {
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T {
        return items[position]
    }

    fun setNewValues(values: List<T>?) {
        items.clear()
        items.addAll(values!!)
        refreshRecycler()
    }

    init {
        this.items.addAll(items!!)
    }
}