package com.weather.app.presentation.customclicklistener

import android.view.View

interface OnItemClickListener {
    fun onItemClickListener(position: Int, view: View?)
}