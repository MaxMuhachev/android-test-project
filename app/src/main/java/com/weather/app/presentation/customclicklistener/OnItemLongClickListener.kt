package com.weather.app.presentation.customclicklistener

import android.view.View

interface OnItemLongClickListener {
    fun onItemLongClickListener(position: Int, view: View?)
}