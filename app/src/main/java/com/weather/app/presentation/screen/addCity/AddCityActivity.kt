package com.weather.app.presentation.screen.addCity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.weather.app.App
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.entity.ResponseWeather
import com.weather.app.presentation.screen.editCities.EditCitiesActivity
import com.weather.app.presentation.screen.whether.MainActivity
import com.weather.app.presentation.screen.whether.WeatherViewModel
import com.weather.app.utilit.I18n
import io.reactivex.rxjava3.disposables.CompositeDisposable
import toothpick.ktp.delegate.inject

class AddCityActivity : Activity(), TextWatcher {

    private lateinit var editText: EditText
    private lateinit var textError: TextView
    private lateinit var resultCity: TextView
    private var compositeDisposable = CompositeDisposable()
    private val viewModel: WeatherViewModel by inject()
    private val addCityViewModel: AddCityViewModel by inject()
    private val weatherDataList: MutableList<ResponseWeather> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_city)
        App.scope().inject(this)

        val backButton = findViewById<ImageView>(R.id.backFromAddCityAct)
        editText = findViewById(R.id.editCityName)
        textError = findViewById(R.id.ErrorMessage)
        resultCity = findViewById(R.id.resultCity)

        editText.addTextChangedListener(this)
        backButton.setOnClickListener {
            this.onClickBack()
        }
        resultCity.setOnClickListener { saveCityAndGoBack() }
    }

    private fun saveCityAndGoBack() {
        if (resultCity.text.isNotEmpty()) {
            addCityViewModel.insertCity(City(resultCity.text.toString(), false))
            startActivity(
                Intent(this, MainActivity::class.java)
                    .putExtra(I18n.KEY_FOR_INTENT, resultCity.text.toString())
            )
        }
    }

    private fun onClickBack() {
        startActivity(Intent(this, EditCitiesActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable) {
        viewModel.getWeatherCity(s.toString())
            .subscribe(
                this::onSuccess,
                this::onError
            )
    }

    private fun onSuccess(responseWeather: ResponseWeather) {
        resultCity.text = responseWeather.cityName
        textError.visibility = View.INVISIBLE
        weatherDataList.add(responseWeather)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
    }
}