package com.weather.app.presentation.screen.addCity

import androidx.lifecycle.ViewModel
import com.weather.app.domain.usecase.whether.CityRepoImpl
import com.weather.app.entity.City
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class AddCityViewModel @Inject constructor(
    private val cityRepoImpl: CityRepoImpl
) : ViewModel() {

    fun getAllCities(): Flowable<MutableList<City>> {
        return cityRepoImpl.getAllCities()
    }

    fun deleteCity(city: City) {
        cityRepoImpl.deleteCity(city)
    }

    fun insertCity(city: City) {
        cityRepoImpl.insertCity(city)
    }

    fun updateFavoriteState(city: City) {
        cityRepoImpl.updateFavoriteState(city)
    }

    fun getCityByName(name: String): List<City> {
        return cityRepoImpl.getCityByName(name)
    }
}