package com.weather.app.presentation.screen.editCities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.presentation.base.BaseAdapter
import com.weather.app.presentation.customclicklistener.OnItemClickListener

class CitiesItemAdapterForEditCities(items: List<City>) :
    BaseAdapter<CitiesItemAdapterForEditCities.CitiesItemViewHolderForEditCities, City>(items) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): CitiesItemViewHolderForEditCities {
        return CitiesItemViewHolderForEditCities(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.activity_edit_cities,
                    parent,
                    false),
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: CitiesItemViewHolderForEditCities, position: Int) {
        holder.bind(getItem(position))
    }

    class CitiesItemViewHolderForEditCities(
        itemView: View,
        private var listener: OnItemClickListener,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val nameCity: TextView = itemView.findViewById(R.id.nameCity)
        private val deleteButton: ImageView = itemView.findViewById(R.id.deleteBtn)
        private val favoriteBtn: ImageView = itemView.findViewById(R.id.favoriteBtn)

        fun bind(item: City) {
            nameCity.text = item.name
            if (item.favorite != null) {
                if (item.favorite) {
                    favoriteBtn.setColorFilter(ContextCompat.getColor(itemView.context,
                        R.color.warning))
                } else {
                    favoriteBtn.setColorFilter(ContextCompat.getColor(itemView.context,
                        R.color.material_on_background_disabled))
                }
            }
        }

        override fun onClick(v: View) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClickListener(position, v)
            }
        }

        init {
            deleteButton.setOnClickListener(this)
            favoriteBtn.setOnClickListener(this)
            nameCity.setOnClickListener(this)
        }
    }
}