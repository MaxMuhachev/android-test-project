package com.weather.app.presentation.screen.editCities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.App
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.presentation.customclicklistener.OnItemClickListener
import com.weather.app.presentation.screen.addCity.AddCityActivity
import com.weather.app.presentation.screen.addCity.AddCityViewModel
import com.weather.app.presentation.screen.whether.MainActivity
import com.weather.app.utilit.I18n
import io.reactivex.rxjava3.disposables.CompositeDisposable
import toothpick.ktp.delegate.inject
import java.util.*

class EditCitiesActivity : AppCompatActivity(), OnItemClickListener {

    private val cityViewModel: AddCityViewModel by inject()

    var compositeDisposable = CompositeDisposable()
    private var cityList: List<City> = ArrayList()
    private lateinit var adapter: CitiesItemAdapterForEditCities

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cities_items_edit)
        App.scope().inject(this)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewEditCities)
        val backButton: ImageView = findViewById(R.id.backBtn)
        val startAddActivity: ImageView = findViewById(R.id.startAddActivity)

        init();
        setupCitiesRecyclerView(recyclerView);

        backButton.setOnClickListener(this::onClickBackButton)
        startAddActivity.setOnClickListener(this::onClickAddButton)
    }

    private fun init() {
        compositeDisposable.add(
            cityViewModel.getAllCities()
                .subscribe(this::onSuccess, this::onError)
        )
    }

    private fun setupCitiesRecyclerView(recyclerView: RecyclerView) {
        adapter = CitiesItemAdapterForEditCities(cityList)
        adapter.onItemClickListener = this
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun onSuccess(cities: List<City>) {
        cityList = cities
        adapter.setNewValues(cities)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    private fun onClickBackButton(v: View) {
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun onClickAddButton(v: View) {
        startActivity(Intent(this, AddCityActivity::class.java))
    }

    override fun onItemClickListener(position: Int, view: View?) {
        when (view!!.id) {
            R.id.deleteBtn -> cityViewModel.deleteCity(cityList[position])
            R.id.favoriteBtn -> {
                val favoriteCity = cityViewModel.getCityByName(cityList[position].name)[0]
                favoriteCity.favorite = !favoriteCity.favorite
                cityViewModel.updateFavoriteState(favoriteCity)
            }
            else -> startActivity(
                Intent(this, MainActivity::class.java)
                    .putExtra(I18n.KEY_FOR_INTENT, cityList[position].name)
            )
        }
    }
}