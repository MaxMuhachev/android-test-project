package com.weather.app.presentation.screen.whether

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.presentation.base.BaseAdapter
import com.weather.app.presentation.customclicklistener.OnItemClickListener
import com.weather.app.presentation.screen.whether.CityItemAdapter.CitiesItemViewHolder

class CityItemAdapter(items: List<City>) : BaseAdapter<CitiesItemViewHolder, City>(items) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesItemViewHolder {
        return CitiesItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_cities,
                    parent,
                    false),
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: CitiesItemViewHolder, position: Int) {
        holder.bind(getItem(position).name)
    }

    class CitiesItemViewHolder(
        itemView: View,
        private val listener: OnItemClickListener,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val nameCity: TextView = itemView.findViewById(R.id.nameCity)

        fun bind(item: String) {
            nameCity.text = item
        }

        override fun onClick(v: View) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClickListener(position, v)
            }
        }

        init {
            nameCity.setOnClickListener(this)
        }
    }
}