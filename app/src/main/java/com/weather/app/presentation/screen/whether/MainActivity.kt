package com.weather.app.presentation.screen.whether

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import com.weather.app.App
import com.weather.app.R
import com.weather.app.entity.City
import com.weather.app.entity.ResponseWeather
import com.weather.app.entity.Weather
import com.weather.app.presentation.customclicklistener.OnItemClickListener
import com.weather.app.presentation.screen.addCity.AddCityActivity
import com.weather.app.presentation.screen.addCity.AddCityViewModel
import com.weather.app.presentation.screen.editCities.EditCitiesActivity
import com.weather.app.utilit.*
import io.reactivex.rxjava3.disposables.CompositeDisposable
import toothpick.ktp.delegate.inject


class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener,
    OnItemClickListener {

    private lateinit var drawerLayout: DrawerLayout
    lateinit var toolbar: Toolbar

    private val items: MutableList<City> = ArrayList()
    private val favoriteItems: MutableList<City> = ArrayList()

    lateinit var adapter: CityItemAdapter
    lateinit var adapterFavoriteItems: CityItemAdapter

    lateinit var city: TextView
    lateinit var degrees: TextView
    lateinit var humidity: TextView
    lateinit var visibility: TextView
    lateinit var clouds: TextView
    lateinit var sunrise: TextView
    lateinit var sunset: TextView
    lateinit var pressure: TextView
    lateinit var wind: TextView
    lateinit var description: TextView
    lateinit var degreesMaxAndMin: TextView
    lateinit var degreesFeelsLike: TextView
    lateinit var addCityAct: ImageView
    private val compositeDisposable = CompositeDisposable()

    private lateinit var imgWeather: ImageView

    private val viewModel: WeatherViewModel by inject()
    private val addCityViewModel: AddCityViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.scope().inject(this)

        city = findViewById(R.id.city)
        degrees = findViewById(R.id.degrees)
        description = findViewById(R.id.description)
        degreesMaxAndMin = findViewById(R.id.degreesMaxAndMin)
        degreesFeelsLike = findViewById(R.id.degreesFeelsLike)
        imgWeather = findViewById(R.id.imgWeather)

        humidity = findViewById(R.id.humidity)
        visibility = findViewById(R.id.visibility)
        clouds = findViewById(R.id.clouds)
        sunrise = findViewById(R.id.sunrise)
        sunset = findViewById(R.id.sunset)
        pressure = findViewById(R.id.pressure)
        wind = findViewById(R.id.wind)
        addCityAct = findViewById(R.id.addCityAct)

        init()

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewActivityMain)
        val recyclerViewWithFavoriteItems =
            findViewById<RecyclerView>(R.id.recyclerViewActivityMainFavorite)
        setupCitiesRecyclerView(recyclerView)
        setupFavoriteCitiesRecyclerView(recyclerViewWithFavoriteItems)
    }

    private fun setupActionBar(
        citiesSettings: Button,
        toolbar: Toolbar,
        navigationView: NavigationView,
    ) {
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
        val actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.openNavBar,
            R.string.closeNavBar
        )

        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)
        citiesSettings.setOnClickListener(this)
    }

    private fun init() {
        compositeDisposable.add(
            addCityViewModel
                .getAllCities()
                .subscribe(this::onSuccessSortFavorite, this::onError)
        )
    }


    private fun onSuccessSortFavorite(cities: List<City>) {
        var city: String? = null
        if (intent.extras != null) {
            city = intent.extras!!.getString(I18n.KEY_FOR_INTENT)
        }
        for (item in cities) {
                if (item.favorite) {
                    favoriteItems.add(item)
                } else {
                    items.add(item)
                }
        }
        adapterFavoriteItems.setNewValues(favoriteItems)
        adapter.setNewValues(items)
        adapter.notifyDataSetChanged()
        adapterFavoriteItems.notifyDataSetChanged()
        compositeDisposable.add(
            viewModel.getWeatherCity(city ?: favoriteItems[0].name)
                .subscribe(this::onSuccessGetWeather, this::onError)
        )
    }

    @SuppressLint("SetTextI18n")
    private fun onSuccessGetWeather(responseWeather: ResponseWeather) {
        val weather: Weather = responseWeather.weather[0]
        toolbar = findViewById(R.id.main_toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)

        Picasso.get().load(weather.getIconLink()).into(imgWeather)
        city.text = NameAndCityFormatter.format(responseWeather)
        degrees.text = DegreesFormatter.format(responseWeather.main.temp)
        description.text = weather.description
        degreesMaxAndMin.text = MaxAndMinDegreesFormatter.format(responseWeather)
        degreesFeelsLike.text =
            getString(R.string.feelsLike)  + " " + DegreesFormatter.format(responseWeather.main.feelsLike)

        humidity.text = PercentFormatter.format(responseWeather.main.humidity)
        visibility.text = VisibilityFormatter.format(responseWeather)
        clouds.text = PercentFormatter.format(responseWeather.clouds.all)
        sunrise.text = DateFormatter.format(responseWeather.sys.sunrise, responseWeather.timezone)
        sunset.text = DateFormatter.format(responseWeather.sys.sunset, responseWeather.timezone)
        pressure.text = PressureFormatter.format(responseWeather)
        wind.text = WindFormatter.format(responseWeather)

        val citiesSettings: Button = findViewById(R.id.citiesSetting)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        setupActionBar(citiesSettings, toolbar, navigationView)
    }

    private fun setupFavoriteCitiesRecyclerView(recyclerViewFavorite: RecyclerView) {
        adapterFavoriteItems = CityItemAdapter(favoriteItems)
        adapterFavoriteItems.onItemClickListener = this
        val linearLayoutManagerFavorite = LinearLayoutManager(this)
        recyclerViewFavorite.layoutManager = linearLayoutManagerFavorite
        recyclerViewFavorite.adapter = adapterFavoriteItems
    }

    private fun setupCitiesRecyclerView(recyclerView: RecyclerView) {
        adapter = CityItemAdapter(items)
        adapter.onItemClickListener = this
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter
    }

    private fun onError(throwable: Throwable) {
        NetworkManager.isNetworkAvailable(this)
        throwable.printStackTrace()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return true
    }

    override fun onClick(v: View?) {
        startActivity(Intent(this, EditCitiesActivity::class.java))
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.addButtonFromMainActivity) {
            val intent = Intent(this, AddCityActivity::class.java)
            startActivity(intent)
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return false
    }

    override fun onItemClickListener(position: Int, view: View?) {
        drawerLayout.closeDrawer(GravityCompat.START)
        compositeDisposable.add(
            viewModel.getWeatherCity((view as TextView).text.toString())
                .subscribe(
                    this::onSuccessGetWeather,
                    this::onError
                )
        )
    }

}