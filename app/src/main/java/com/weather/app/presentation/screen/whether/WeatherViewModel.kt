package com.weather.app.presentation.screen.whether

import androidx.lifecycle.ViewModel
import com.weather.app.domain.usecase.whether.GetWeather
import com.weather.app.entity.ResponseWeather
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class WeatherViewModel @Inject constructor(
    private val getWeather: GetWeather
) : ViewModel() {

    fun getWeatherCity(q: String): Single<ResponseWeather> {
        return getWeather.invoke(q);
    }
}