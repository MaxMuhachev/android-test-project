package com.weather.app.utilit

import java.text.SimpleDateFormat
import java.util.*

class DateFormatter {
    companion object {
        fun format(dateTime: Int, timezone: Int): String {
            return SimpleDateFormat(I18n.FORMAT_TIME, Locale.US)
                .format(convertFromUnixInDate(dateTime, timezone))
        }

        private fun convertFromUnixInDate(dt: Int, timezone: Int): Date {
            return Date((dt + timezone) * 1000L)
        }
    }
}