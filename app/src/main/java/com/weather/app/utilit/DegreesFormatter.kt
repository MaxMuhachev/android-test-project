package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather
import kotlin.math.roundToInt

class DegreesFormatter {
    companion object {
        fun format(degrees: Float): String {
            return degrees.roundToInt().toString() + I18n.DEGREES
        }
    }

}