package com.weather.app.utilit;

public class I18n {
    public static final String NAME_CITY_PERM_RU = "Пермь";
    public static final String NAME_TEST_CITY_BANG_RU = "Бангкок";

    public static final String NAME_DATABASE = "cities";
    public static final String KEY_FOR_INTENT = "city";

    public static final String FORMAT_TIME = "h:mm a";
    public static final String DEGREES = "°";

    public static final String NO_NETWORK = "Нет соединения c интернетом.\nПопробуйте похзже";

}
