package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather
import kotlin.math.roundToInt

class MaxAndMinDegreesFormatter {
    companion object {
        fun format(responseWeather: ResponseWeather): String {
            return "макс " + responseWeather.main.tempMax.roundToInt() + I18n.DEGREES + " / мин " + responseWeather.main.tempMin.roundToInt() + I18n.DEGREES
        }
    }
}
