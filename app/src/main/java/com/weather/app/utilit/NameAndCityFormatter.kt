package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather

class NameAndCityFormatter {
    companion object {
        fun format(responseWeather: ResponseWeather): String {
            return responseWeather.cityName + ", " + responseWeather.sys.country
        }
    }

}