package com.weather.app.utilit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.widget.Toast;

public class NetworkManager {
    public static void isNetworkAvailable(Context context) {
        Toast toastNoNetwork = Toast.makeText(context, I18n.NO_NETWORK, Toast.LENGTH_SHORT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                Network activeNetwork = connectivityManager.getActiveNetwork();
                NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                if (networkCapabilities != null) {
                    boolean wiFi = networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
                    boolean cellular = networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR);
                    boolean internet = networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET);
                    if (!wiFi && !cellular && !internet) {
                        toastNoNetwork.show();
                    }
                } else {
                    toastNoNetwork.show();
                }
            }
        }
    }
}
