package com.weather.app.utilit

class PercentFormatter {
    companion object {
        fun format(number: Int): String {
            return "$number%"
        }
    }

}