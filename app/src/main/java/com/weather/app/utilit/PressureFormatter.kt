package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather
import kotlin.math.floor
import kotlin.math.roundToInt

class PressureFormatter {
    companion object {
        fun format(responseWeather: ResponseWeather): String {
            return (responseWeather.main.pressure * 0.75006375541921).roundToInt().toString() + " мм.рт.ст."
        }
    }

}