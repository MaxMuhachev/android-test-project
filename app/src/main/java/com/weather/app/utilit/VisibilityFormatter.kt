package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather
import kotlin.math.roundToInt

class VisibilityFormatter {
    companion object {
        fun format(responseWeather: ResponseWeather): String {
            return (responseWeather.visibility / 1000).toString() + " км";
        }
    }

}