package com.weather.app.utilit

import com.weather.app.entity.ResponseWeather
import kotlin.math.roundToInt

class WindFormatter {
    companion object {
        fun format(responseWeather: ResponseWeather): String {
            return responseWeather.wind.speed.toString() + " м/с";
        }
    }

}