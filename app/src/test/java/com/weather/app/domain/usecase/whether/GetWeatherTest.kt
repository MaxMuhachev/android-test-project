package com.weather.app.domain.usecase.whether

import com.weather.app.BuildConfig
import com.weather.app.entity.ResponseWeather
import com.weather.app.utilit.I18n
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetWeatherTest {

    @InjectMocks
    lateinit var getWeather: GetWeather

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun invoke() {
        val testObserver: TestObserver<ResponseWeather> =
            getWeather.invoke(BuildConfig.NAME_CITY_PERM)
                .test()
        testObserver.await()

        testObserver
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue { value -> value.cityName == I18n.NAME_CITY_PERM_RU }
        testObserver.dispose()
    }
}